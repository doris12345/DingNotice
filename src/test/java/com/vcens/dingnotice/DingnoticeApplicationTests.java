package com.vcens.dingnotice;

import com.alibaba.fastjson.JSON;
import com.vcens.dingnotice.entity.TaskEntity;
import com.vcens.dingnotice.service.impl.TaskerServiceImpl;
import com.vcens.dingnotice.utils.DataResult;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DingnoticeApplicationTests {
    @Autowired
    private TaskerServiceImpl taskerService;

    @Test
    public void create() {
        try {
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setId(1);
            taskEntity.setName("CreateTestRunner");
            taskEntity.setGroup("CreateTestRunner");
            taskEntity.setDesc("创建测试任务");
            taskEntity.setClassName("com.vcens.dingnotice.task.init.DingMessageTask");
            taskEntity.setCronExpression("*/5 * * * * ?");
            taskerService.buildTask(taskEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void list(){
        List<Map<String, Object>> list = taskerService.list();
        System.out.println(JSON.toJSONString(list));
    }

}
