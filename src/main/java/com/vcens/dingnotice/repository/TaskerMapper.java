package com.vcens.dingnotice.repository;

import com.vcens.dingnotice.entity.TaskEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TaskerMapper {

    @Select("select * from task_infos limit #{cp},#{pageSize}")
    List<TaskEntity> listAllByPage(Integer cp, Integer pageSize);

    @Select("select * from task_infos where trigger_state = #{trigger_state}")
    List<TaskEntity> listAllByState(@Param("trigger_state") Integer triggerState);

}
