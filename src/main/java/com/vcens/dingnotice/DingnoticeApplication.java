package com.vcens.dingnotice;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.vcens.dingnotice")
@EnableTransactionManagement(proxyTargetClass = true)
@Slf4j
public class DingnoticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DingnoticeApplication.class, args);
    }
}
