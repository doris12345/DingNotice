package com.vcens.dingnotice.rest.v1;

import com.vcens.dingnotice.entity.TaskEntity;
import com.vcens.dingnotice.exception.RestException;
import com.vcens.dingnotice.service.impl.TaskerServiceImpl;
import com.vcens.dingnotice.utils.DataResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author vechar
 */
@RestController
@Slf4j
@RequestMapping("/api/v1.0/task")
public class MainController{
    @Autowired
    private TaskerServiceImpl taskerService;

    /**
     * 创建钉钉消息通知任务
     *
     * @param taskEntity
     * @return
     */
    @PostMapping("/create")
    public DataResult create(TaskEntity taskEntity) throws RestException {
//            TaskEntity taskEntity = new TaskEntity();
//            taskEntity.setId(1);
//            taskEntity.setName("CreateTestRunner");
//            taskEntity.setGroup("CreateTestRunner");
//            taskEntity.setDesc("创建测试任务");
//            taskEntity.setClassName("com.vcens.dingnotice.task.init.DingMessageTask");
//            taskEntity.setCronExpression("*/5 * * * * ?");
            taskerService.buildTask(taskEntity);

        return null;
    }

    /**
     * 任务列表
     *
     * @param taskEntity
     * @param cp
     * @param pageSize
     * @return
     */
    @PostMapping("/list")
    public DataResult list(TaskEntity taskEntity, Integer cp, Integer pageSize) throws RestException {
//        List<TaskEntity> list = taskerService.listByPage(taskEntity, cp, pageSize);
        List<Map<String,Object>> list = taskerService.list();
        return DataResult.ok(list);
    }

    /**
     * 开始任务
     *
     * @param taskEntity
     * @return
     */
    @PostMapping("/trigger")
    public DataResult trigger(TaskEntity taskEntity) throws RestException {
        taskerService.triggerTask(taskEntity);
        return DataResult.ok();
    }

    /**
     * 暂停任务
     *
     * @param taskEntity
     * @return
     */
    @PostMapping("/pause")
    public DataResult pause(TaskEntity taskEntity) throws RestException {
        taskerService.pauseTask(taskEntity);
        return DataResult.ok();
    }

    /**
     * 恢复任务
     *
     * @param taskEntity
     * @return
     */
    @PostMapping("/resume")
    public DataResult resume(TaskEntity taskEntity) throws RestException {
        taskerService.resumeTask(taskEntity);
        return null;
    }

    /**
     * 删除任务
     *
     * @param taskEntity
     * @return
     */
    @PostMapping("/remove")
    public DataResult remove(TaskEntity taskEntity) throws RestException {
            taskerService.removeTask(taskEntity);

        return null;
    }
}
