package com.vcens.dingnotice.exception;

/**
 * @Author VeChar
 * @Desc
 * @CreateTime 2018/9/29 11:54 PM
 **/
public class ViewException extends Exception {
    public ViewException(String message) {
        super(message);
    }
}
