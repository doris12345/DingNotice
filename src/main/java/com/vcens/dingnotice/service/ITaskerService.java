package com.vcens.dingnotice.service;

import com.vcens.dingnotice.entity.TaskEntity;
import com.vcens.dingnotice.exception.RestException;
import org.quartz.SchedulerException;

import java.util.List;
import java.util.Map;

/**
 * @author vechar
 */
public interface ITaskerService {
    void buildTask(TaskEntity taskEntity) throws RestException;

    TaskEntity findById(Object jobid) throws RestException;

    void triggerTask(TaskEntity taskEntity) throws RestException;

    List<TaskEntity> listByPage(TaskEntity taskEntity, Integer cp, Integer pageSize);

    void pauseTask(TaskEntity taskEntity) throws RestException;

    void resumeTask(TaskEntity taskEntity) throws RestException;

    void removeTask(TaskEntity taskEntity) throws RestException;

    List<Map<String, Object>> list() throws RestException;
}
