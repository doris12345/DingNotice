package com.vcens.dingnotice.service;

import com.vcens.dingnotice.entity.MessageEntity;

public interface IMessageContentService {
    MessageEntity findByJobId(Integer jobId);
}
