package com.vcens.dingnotice.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @Author VeChar
 * @Desc    Bing每日背景图获取
 * @CreateTime 2018/9/29 15:54 PM
 **/
@Slf4j
public class BackgroundImageUtils {
    /**
     * 获取背景图的地址
     */
    private static final String URI = "https://cn.bing.com/HPImageArchive.aspx?format=js&idx=1&n=8";
    private static final String BASE_URI = "https://www.bing.com";

    public static Map<String, Object> getImages() {
        try {
            List<Map<String, Object>> datas = new ArrayList<>();
            String s = HttpRequest.httpPost(URI, null, null, "UTF-8");
            Map<String, Object> map = (Map<String, Object>) JSONObject.parse(s);
            List imageList = JSONObject.parseArray(map.get("images").toString());
            if (imageList.size() < 0) return null;
            for (Object o : imageList) {
                Map<String, Object> m = (Map<String, Object>) o;
                Map<String, Object> image = new HashMap<>();
                image.put("imgUrl", BASE_URI + m.get("url"));
                image.put("descUrl", m.get("copyrightlink"));
                datas.add(image);
            }
            return datas.get(new Random().nextInt(8));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
