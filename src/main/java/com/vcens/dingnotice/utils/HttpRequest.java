package com.vcens.dingnotice.utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.*;
/**
 * @Author VeChar
 * @Desc    Http请求工具类
 * @CreateTime 2016/8/5 06:03 AM
 **/
public class HttpRequest {
    /**
     * 处理get请求.
     *
     * @param url         请求路径
     * @param headerParam 请求头参数
     * @param params      请求参数
     * @return
     */
    public static String httpGet(String url, Map<String, String> headerParam, Map<String, String> params, String CharSet) {
        //实例化httpclient
        CloseableHttpClient httpclient = HttpClients.createDefault();

        // Url参数拼接
        if (params != null && params.size() > 0) {
            Iterator<String> it = params.keySet().iterator();
            StringBuffer sb = null;
            while (it.hasNext()) {
                String key = it.next();
                String value = params.get(key);
                if (sb == null) {
                    sb = new StringBuffer();
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                sb.append(key);
                sb.append("=");
                sb.append(value);
            }
            url += sb.toString();
        }

        //实例化get方法
        HttpGet httpGet = new HttpGet(url);

        //设置请求头信息
        if (headerParam != null && headerParam.size() > 0) {
            for (String key : headerParam.keySet()) {
                httpGet.setHeader(key, headerParam.get(key));
            }
        }
        //请求结果
        CloseableHttpResponse response = null;
        String content = "";
        try {
            //执行get方法
            response = httpclient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                content = EntityUtils.toString(response.getEntity(), CharSet);
                System.out.println(content);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    /**
     * 处理post请求
     *
     * @param url         请求url
     * @param headerParam 请求头参数信息
     * @param params      请求参数信息
     * @return
     */
    public static String httpPost(String url, Map<String, String> headerParam, Map<String, String> params, String CharSet) {
        //实例化httpClient
        CloseableHttpClient httpclient = HttpClients.createDefault();
        //实例化post方法
        HttpPost httpPost = new HttpPost(url);

        //处理参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        if (params != null && params.size() > 0) {
            Set<String> keySet = params.keySet();
            for (String key : keySet) {
                nvps.add(new BasicNameValuePair(key, params.get(key)));
            }
        }

        //设置请求头信息
        if (headerParam != null && headerParam.size() > 0) {
            for (String key : headerParam.keySet()) {
                httpPost.setHeader(key, headerParam.get(key));
            }
        }
        //结果
        CloseableHttpResponse response = null;
        String content = "";
        try {
            //将参数给post方法
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, CharSet));
            //执行post方法
            response = httpclient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == 200) {
                content = EntityUtils.toString(response.getEntity(), CharSet);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
