package com.vcens.dingnotice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskEntity {
    private Integer id;
    private String name;//任务名称
    private String group;//任务分组
    private String desc;//任务描述
    private String className;//执行类
    private String cronExpression;//执行时间
    private String triggerTime;//执行时间
    private String triggerState;//任务状态
    private Date createTime;
    private Date updateTime;
}
