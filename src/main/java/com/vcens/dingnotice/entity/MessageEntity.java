package com.vcens.dingnotice.entity;

import lombok.Data;

import java.util.Date;

@Data
public class MessageEntity {
    private Integer id;
    private Integer jobId;
    private String content;    /*这里由于设置的消息内从字段不固定，所以置为JSON格式*/
    private Date createTime;
    private Date updateTime;
}
