package com.vcens.dingnotice.task.init;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.Serializable;
/**
 * @Author VeChar
 * @Desc    初始化测试
 * @CreateTime 2018/9/29 11:54 PM
 **/
@Slf4j
public class InitTask extends QuartzJobBean implements Serializable {
    private static final long serialVersionUID = 5925315723246862647L;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info(this.getClass().getSimpleName() + " has been started...");
        log.info("初始化任务执行...");
    }
}
