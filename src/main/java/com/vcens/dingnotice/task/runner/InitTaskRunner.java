package com.vcens.dingnotice.task.runner;

import com.vcens.dingnotice.entity.TaskEntity;
import com.vcens.dingnotice.service.impl.TaskerServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
/**
 * @Author VeChar
 * @Desc
 * @CreateTime 2018/9/29 11:54 PM
 **/
@Slf4j
@Component
public class InitTaskRunner implements ApplicationRunner {

    @Autowired
    private TaskerServiceImpl taskerService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setName("InitTestRunner");
        taskEntity.setGroup("InitTestRunner");
        taskEntity.setDesc("初始化测试任务");
        taskEntity.setClassName("com.vcens.dingnotice.task.init.InitTask");
        taskEntity.setCronExpression("*/30 * * * * ?");
        taskerService.buildTask(taskEntity);
    }
}
