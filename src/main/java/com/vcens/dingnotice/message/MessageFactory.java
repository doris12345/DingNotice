package com.vcens.dingnotice.message;

import com.vcens.dingnotice.message.dingtalk.DingMessageFactory;

/**
 * @Author VeChar
 * @Desc    消息工厂
 * @CreateTime 2018/9/29 11:54 PM
 **/
public interface MessageFactory {
    DingMessageFactory getMessageType(Integer taskId);
}
