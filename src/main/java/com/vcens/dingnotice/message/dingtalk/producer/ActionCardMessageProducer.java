package com.vcens.dingnotice.message.dingtalk.producer;

import com.dingtalk.chatbot.message.ActionButtonStyle;
import com.dingtalk.chatbot.message.ActionCardAction;
import com.dingtalk.chatbot.message.ActionCardMessage;
import com.dingtalk.chatbot.message.Message;
import com.vcens.dingnotice.entity.MessageEntity;
import com.vcens.dingnotice.service.impl.MessageContentServiceImpl;
import com.vcens.dingnotice.message.dingtalk.DingMessageFactory;
import com.vcens.dingnotice.utils.BackgroundImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

@Component
public class ActionCardMessageProducer implements DingMessageFactory {

    @Autowired
    private MessageContentServiceImpl messageContentService;

    @Override
    public Message buildMessage(Integer jobId) {
        //拿到消息内容
//        MessageEntity messageEntity = messageContentService.findByJobId(jobId);
        MessageEntity messageEntity = null;
        switch (2) {
            case 1: {
                return sendActionCardMessage(messageEntity);
            }
            case 2: {
                return afterWorkNotice(null);
            }
            case 3: {
                return sendActionCardMessageWithTitle(messageEntity);
            }
            case 4: {
                return sendActionCardMessageWithBanner(messageEntity);
            }
            case 5: {
                return sendActionCardMessageWithHorizontalButton(messageEntity);
            }
            case 6: {
                return sendActionCardMessageWithHorizontalButtonWithoutAvatar(messageEntity);
            }
        }
        return sendActionCardMessage(messageEntity);
    }

    public Message sendActionCardMessage(MessageEntity messageEntity) {
        ActionCardMessage message = new ActionCardMessage();
        message.setBriefText("亲，小秘没有看懂你的问题哦，换个说法问问小秘看~你也可以试试以下问题");
        ActionCardAction action1 = new ActionCardAction("考勤打卡", "http://www.dingtalk.com");
        message.addAction(action1);
        ActionCardAction action2 = new ActionCardAction("办公电话", "http://www.dingtalk.com");
        message.addAction(action2);
        ActionCardAction action3 = new ActionCardAction("智能客服", "http://www.dingtalk.com");
        message.addAction(action3);
        ActionCardAction action4 = new ActionCardAction("更多问题", "http://www.dingtalk.com");
        message.addAction(action4);
        return message;
    }

    public Message afterWorkNotice(MessageEntity messageEntity) {
        //在这里判断消息类型
        ActionCardMessage message = new ActionCardMessage();
        Map<String, Object> image = BackgroundImageUtils.getImages();
        message.setBannerURL(image.get("imgUrl").toString());
        message.setTitle("下班打卡提醒");
        message.setBriefText("忙碌了一天，终于下班了，记得打卡哟~");
        ActionCardAction action1 = new ActionCardAction("了解背景", image.get("descUrl").toString());
        message.addAction(action1);
        ActionCardAction action2 = null;
        try {
            action2 = new ActionCardAction("不感兴趣", URLEncoder.encode("dtmd://dingtalkclient/sendMessage?content=不感兴趣", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        message.addAction(action2);
        message.setActionButtonStyle(ActionButtonStyle.VERTICAL);
        return message;
    }

    public Message sendActionCardMessageWithTitle(MessageEntity messageEntity) {
        ActionCardMessage message = new ActionCardMessage();
        message.setTitle("钉钉功能推荐");
        message.setBriefTitle("创建团队，让工作沟通更加安全高效");
        message.setBriefText("国内广受认可的企业级应用市场入驻标准高、产品质量高，入驻即代表得到市场认可，实力获得钉钉背书");
        ActionCardAction action1 = new ActionCardAction("邀请群成员创建团队", "http://www.dingtalk.com");
        message.addAction(action1);
        return message;
    }

    public Message sendActionCardMessageWithBanner(MessageEntity messageEntity) {
        ActionCardMessage message = new ActionCardMessage();
        message.setTitle("钉钉功能推荐");
        message.setBannerURL("http://img01.taobaocdn.com/top/i1/LB1GCdYQXXXXXXtaFXXXXXXXXXX");
        message.setBriefTitle("创建团队，让工作沟通更加安全高效");
        message.setBriefText("国内广受认可的企业级应用市场入驻标准高、产品质量高，入驻即代表得到市场认可，实力获得钉钉背书");
        ActionCardAction action1 = new ActionCardAction("查看详情", "http://www.dingtalk.com");
        message.addAction(action1);
        ActionCardAction action2 = new ActionCardAction("不感兴趣", "http://www.dingtalk.com");
        message.addAction(action2);
        return message;
    }

    public Message sendActionCardMessageWithHorizontalButton(MessageEntity messageEntity) {
        ActionCardMessage message = new ActionCardMessage();
        message.setBannerURL("http://img01.taobaocdn.com/top/i1/LB1GCdYQXXXXXXtaFXXXXXXXXXX");
        message.setTitle("创建团队，让工作沟通更加安全高效");
        message.setBriefText("国内广受认可的企业级应用市场入驻标准高、产品质量高，入驻即代表得到市场认可，实力获得钉钉背书");
        ActionCardAction action1 = new ActionCardAction("查看详情", "http://www.dingtalk.com");
        message.addAction(action1);
        ActionCardAction action2 = new ActionCardAction("不感兴趣", "http://www.dingtalk.com");
        message.addAction(action2);
        message.setActionButtonStyle(ActionButtonStyle.HORIZONTAL);
        return message;
    }

    public Message sendActionCardMessageWithHorizontalButtonWithoutAvatar(MessageEntity messageEntity) {
        ActionCardMessage message = new ActionCardMessage();
        message.setBannerURL("http://img01.taobaocdn.com/top/i1/LB1GCdYQXXXXXXtaFXXXXXXXXXX");
        message.setTitle("创建团队，让工作沟通更加安全高效");
        message.setBriefText("国内广受认可的企业级应用市场入驻标准高、产品质量高，入驻即代表得到市场认可，实力获得钉钉背书");
        ActionCardAction action1 = new ActionCardAction("查看详情", "dtmd://dingtalkclient/sendMessage?content=world");
        message.addAction(action1);
        ActionCardAction action2 = null;
        try {
            action2 = new ActionCardAction("不感兴趣", URLEncoder.encode("dtmd://dingtalkclient/sendMessage?content=不感兴趣", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        message.addAction(action2);
        message.setActionButtonStyle(ActionButtonStyle.HORIZONTAL);
        message.setHideAvatar(true);
        return message;
    }
}
