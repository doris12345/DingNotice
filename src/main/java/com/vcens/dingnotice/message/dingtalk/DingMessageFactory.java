package com.vcens.dingnotice.message.dingtalk;

import com.dingtalk.chatbot.message.Message;

/**
 * @Author VeChar
 * @Desc 钉钉消息工厂
 * @CreateTime 2018/9/29 11:54 PM
 **/
public interface DingMessageFactory {
    /**
     * 创建消息
     *
     * @param jobId 任务ID
     * @return
     */
    Message buildMessage(Integer jobId);
}
