package com.vcens.dingnotice.message;

import com.vcens.dingnotice.message.dingtalk.DingMessageFactory;
import com.vcens.dingnotice.message.dingtalk.producer.ActionCardMessageProducer;
import com.vcens.dingnotice.service.impl.TaskerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author VeChar
 * @Desc
 * @CreateTime 2018/9/29 15:34 PM
 **/
public class MessageProducer implements MessageFactory {

    @Autowired
    private TaskerServiceImpl taskerService;

    @Override
    public DingMessageFactory getMessageType(Integer taskId) {
        //根据JOBID去查询消息类型，返回相关的消息卡片工厂对象
//        TaskEntity taskEntity = taskerService.findById(taskId);
        if (taskId == 1) {
            return new ActionCardMessageProducer();
        } else if (taskId == 2) {
            return new ActionCardMessageProducer();
        } else {
            return null;
        }
    }
}
