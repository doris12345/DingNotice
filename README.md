# DingNotice

#### 项目介绍

    通过钉钉E应用或WEB端管理的定时提醒任务系统，提醒到钉钉群，目前仅提醒到钉钉群，
    后期考虑其他任务动作，如：响应Hooks等...,可修改，创建，删除，暂停，恢复等操
    作。
    
    等等，逐步完善
    
####软件架构
    
    钉钉E应用 + SpringBoot2.05 + QuartZ + MySQL + MyBatis(注解式) + VUE
    
    提醒功能依赖钉钉机器人发送卡片消息。

####运行截图

![定时提醒](http://wx2.sinaimg.cn/mw690/becb2e03gy1fvqw6d8x7yj20yi1pcnpd.jpg "DingNotice")
